package it.elialombardi.portafoglio;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import it.elialombardi.portafoglio.database.MoneyFlow;
import it.elialombardi.portafoglio.database.MoneyFlowRepository;

public class MoneyFlowViewModel extends AndroidViewModel {
    private MoneyFlowRepository mRepository;
    private LiveData<List<MoneyFlow>> mAllMoneyFlows;

    public MoneyFlowViewModel (Application application) {
        super(application);
        mRepository = new MoneyFlowRepository(application);
        mAllMoneyFlows = mRepository.getAllMoneyFlows();
    }

    public LiveData<List<MoneyFlow>> getAllMoneyFlows() {
        return mAllMoneyFlows;
    }

    public void insert(MoneyFlow moneyFlow) { mRepository.insert(moneyFlow); }
}
