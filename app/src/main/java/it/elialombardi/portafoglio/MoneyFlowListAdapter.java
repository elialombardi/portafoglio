package it.elialombardi.portafoglio;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import it.elialombardi.portafoglio.database.MoneyFlow;

public class MoneyFlowListAdapter extends RecyclerView.Adapter<MoneyFlowListAdapter.MoneyFlowViewHolder> {
    class MoneyFlowViewHolder extends RecyclerView.ViewHolder {
        private final TextView amountTextView;
        private final TextView titleTextView;
        private final TextView dateTextView;

        public MoneyFlowViewHolder(@NonNull View itemView) {
            super(itemView);
            this.amountTextView = itemView.findViewById(R.id.money_flow_amount);
            this.titleTextView = itemView.findViewById(R.id.money_flow_title);
            this.dateTextView = itemView.findViewById(R.id.money_flow_date);
        }
    }
    private static SimpleDateFormat sdFotmatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.ITALY);

    @NonNull
    @Override
    public MoneyFlowViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, viewGroup,false);
        return new MoneyFlowViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MoneyFlowViewHolder moneyFlowViewHolder, int i) {
        Resources res = moneyFlowViewHolder.itemView.getContext().getResources();
        if(mMoneyFlows != null) {
            MoneyFlow current = mMoneyFlows.get(i);
            moneyFlowViewHolder.amountTextView.setText(NumberFormat.getCurrencyInstance().format(current.getAmount()));
            moneyFlowViewHolder.dateTextView.setText(sdFotmatter.format(current.getDate()));
            moneyFlowViewHolder.titleTextView.setText(current.getTitle());
        } else {
            moneyFlowViewHolder.amountTextView.setText(res.getString(R.string.no_money_flow));
        }
    }

    void setMoneyFlows(List<MoneyFlow> moneyFlows) {
        mMoneyFlows = moneyFlows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(mMoneyFlows != null)
            return mMoneyFlows.size();
        else
            return 0;
    }


    private final LayoutInflater mInflater;
    private List<MoneyFlow> mMoneyFlows; // Cached copy

    public MoneyFlowListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }
}
