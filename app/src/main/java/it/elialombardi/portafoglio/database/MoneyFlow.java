package it.elialombardi.portafoglio.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Date;

@Entity(tableName = "money_flows")
public class MoneyFlow {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private BigDecimal amount;

    @NonNull
    private Date date;


    private String title;


    public MoneyFlow(BigDecimal amount, String title) {
        this.amount = amount;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getTitle() {
        return title;
    }

    public Date getDate() {
        return date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(@NonNull Date date) {
        this.date = date;
    }
}
