package it.elialombardi.portafoglio.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class MoneyFlowRepository {
    private MoneyFlowDao mMoneyFlowDao;
    private LiveData<List<MoneyFlow>> mAllMoneyFlows;

    public MoneyFlowRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        this.mMoneyFlowDao = db.moneyFlowDao();
        this.mAllMoneyFlows = mMoneyFlowDao.getAllMoneyFlows();
    }

    public LiveData<List<MoneyFlow>> getAllMoneyFlows() {
        return mAllMoneyFlows;
    }

    public void insert (MoneyFlow moneyFlow) {
        new insertAsyncTask(mMoneyFlowDao).execute(moneyFlow);
    }

    private static class insertAsyncTask extends AsyncTask<MoneyFlow, Void, Void> {

        private MoneyFlowDao mAsyncTaskDao;

        insertAsyncTask(MoneyFlowDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MoneyFlow... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
